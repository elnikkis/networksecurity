# ネットワークセキュリティ

## 実験用サーバ

133.15.41.21

port:
10004 Webサーバのssh
20004 Webサーバのhttpd
30004 攻撃サーバのssh

Webサーバのユーザ名 p3-web004
攻撃サーバのユーザ名 p3-atk004


Webサーバ
192.168.1.104
攻撃サーバ
192.168.2.104


### passwd

passwd
p3-web004
hironaka
hironaka


## iptables

攻撃サーバからWebサーバへwgetしてみる
攻撃サーバから見ると、WebサーバはローカルIP&port80にみえる

iptables -A INPUT -p tcp --dport 80 -j DROP

## 5.1

攻撃するコマンド
`sudo curl-loader -f atk100.cnf

## 5.1.2
１秒間に最大150アクセス
最低2とか
平均１００アクセスくらい

`cat access100.log | awk -F" " '{if($1 != "::1"} print $4}' | sort | uniq -c`

## フィルタリング

awk -F" " '{if($1 != "::1") print $4 " " $1}' | sort | uniq -c | awk '{if($1>=4) print $3}'

## 調査課題

### 課題1 suではなくsudoを使う利点

rootのパスワードを管理者権限を使うすべての人が知っていなくて済む

rootのパスワードを変更したとき、ほかの管理者に伝達するという面倒がない


### 課題2
自分にはすべての権限を、グループ・ほかのユーザには読み込み権限だけを与えるchmodコマンドを考察せよ。また、chownコマンドにより変更できるユーザはどういったユーザかを調査せよ

chmod 744

chown 変更を行えるのは、そのファイルの所有者または管理者だけ


### 課題3
2.1節で示したDoS攻撃から3つを選択し、各攻撃の具体的な方法・特徴・代表的な対策方法を調査せよ


### 課題4
DDoS攻撃は阻止することが難しいとされているが、その理由を調査せよ


### 課題5
ネットワーク攻撃の検出もしくは対策に関する英語論文を読み、概要・手法・実験をまとめ、報告・考察せよ。英語論文は４ページ以上のものとし、出典を必ず明記すること


レポート

表紙の概要abst
目的、やったこと、結果

サービス妨害攻撃(Denial of Service Attack, DoS Attack)、そのうちでもHTTP GET Flood攻撃の基本的な対策を行い、ネットワークセキュリティの基礎知識およびDoS攻撃対策の基礎知識を学んだ。
実験では、まず、HTTP GET Flood攻撃によるサーバの負荷を調べるため、基礎実験を行った。
次に、攻撃へネットワークフィルタリングの対策を行い、サーバの負荷の変化を調べた。
また、DoS攻撃がもたらす脅威について考察した。
結果、

1. 目的

2. 基礎知識
2.1 DoS攻撃
2.2 HTTP GET Flood攻撃への対策
DoS攻撃についてやHTTP GET flood


3. 実験

3.1 実験環境
/cat/proc/cpuinfo
ipアドレスとか
ネットワーク構成（ルータがどうなっているか）

実験内容
3.2 基礎実験
3.3 攻撃者判定実験

4. 実験結果
4.1 基礎実験
4.2 攻撃者判定実験


5. シェルスクリプト課題

6. 調査課題

参考文献


# 実験手順
iptablesをクリアする
メモリクリア
apacheサーバ再起動
監視開始、ログ記録開始
攻撃開始

