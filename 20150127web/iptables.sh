#!/bin/sh

# ルール全削除
iptables -F

# ポリシー(ALL ACCEPT)
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT

iptables -A INPUT -p tcp --dport 80 -j DROP

