#!/bin/sh

OUTPUT_FILE="cpudef400.log"
ACCESS_FILE="accdef400.log"

# reset server
/etc/init.d/apache2 restart
sysctl -w vm.drop_caches=3

# record
vmstat -n 5 30 | tee $OUTPUT_FILE

cp /var/log/apache2/p3/access.log $ACCESS_FILE

