#!/bin/sh

# vmstatの出力結果をcsvにする
# usage: cat cpudef800.log | sh makecsv.sh > dpudef400.csv

awk -F" " '{ printf("%s,%s,%s\n", $1, $4, $13) }'
