#!/bin/sh

#INFILE="/var/log/apache2/p3/access.log"
INFILE="../accdef800.log"
BLACKFILE="BLACKLISTS"
TMP="_tmp"
SORTED="_sorted"

echo -n > $BLACKFILE
iptables -F

while :
do
    sleep 2
    num=1
    cat $INFILE | awk -F" " '{if($1 != "::1") print $4 " " $1}' | sort | uniq -c | awk '$1>=4{print $3}' | sort | uniq > $TMP
    sort -m $BLACKFILE $TMP > $SORTED
    cat $SORTED | uniq -u | while read line
    do
        echo $num: $line
        num=$((num+1))
        iptables -A INPUT -p tcp -s $line -j DROP
    done
    cat $SORTED | uniq > $BLACKFILE
done

