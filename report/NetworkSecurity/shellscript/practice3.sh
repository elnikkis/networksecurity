#!/bin/sh

# record time

echo -n > time_record.txt
count=0
while [ $count -ne 60 ]; do
    date >> time_record.txt
    count=`expr $count + 1`
    sleep 1
done
