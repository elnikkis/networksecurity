# ネットワークセキュリティ

## 実験用サーバ

133.15.41.21

port:
10004 Webサーバのssh
20004 Webサーバのhttpd
30004 攻撃サーバのssh

Webサーバのユーザ名 p3-web004
攻撃サーバのユーザ名 p3-atk004


Webサーバ
192.168.1.104
攻撃サーバ
192.168.2.104


### passwd

passwd
p3-web004
hironaka
hironaka


## iptables

攻撃サーバからWebサーバへwgetしてみる
攻撃サーバから見ると、WebサーバはローカルIP&port80にみえる


## 実験

### サーバの初期化

sudo /etc/init.d/apache2 restart
sudo sysctl -w vm.drop_caches=3


### 手順

TIMER_AFTER_URL_SLEEPの値を100, 200, 400にした3通り行う

フィルタリングを行った場合と行わなかった場合をそれぞれ測定
iptablesのdrop
1秒間に4回以上のアクセスを行うものを攻撃者とする
/var/log/apache2/p3/access.logを利用する
attacker_searchというディレクトリを作成し、攻撃者判定に用いたファイルをそこに保存する

サーバの状態確認
vmstat 5 30

## 調査課題

### suではなくsudoを使う利点

rootのパスワードを管理者権限を使うすべての人が知っていなくて済む




